This project is a hacked together object recognition program built from various out of date tutorials and a lot of forum questions/answers. It uses Python and OpenCV.

Currently our solution for recognizing images is to use a Google Drive folder to grab the images. Then the Python script runs to see if the object is recognized. Then the Google Drive emails the result back to the user.


Some of the resources we used:

http://opencv-python-tutroals.readthedocs.org/en/latest/py_tutorials/py_tutorials.html

http://docs.opencv.org/modules/highgui/doc/user_interface.html


Our Google Drive folders (may need an invitation to view/edit):

Uploads:
https://bitbucket.org/dallasbrittany/sciencehackday/src

Originals:
https://drive.google.com/drive/folders/0B_3WLJ5rcBWdfk5jQ1dyME9odEFaMVh6ZE9YWjF1TGFwaEhJLW1yR2l3Q05mWnZpWnpLNzQ


Next Steps:
Create an Android app that uses the Google API to access the Google Drive with MIT App Inventor.
(Info here: http://puravidaapps.com/drive.php)


Contributions:

Bryan came up with the idea and did the majority of the coding and debugging.
Dallas did a lot of debugging and some fixing and set up this repo.
Emily came up with a fun application for this software and took most of the pictures and Photoshopped them to remove the backgrounds.
She also wrote up the scavenger hunt clues.
