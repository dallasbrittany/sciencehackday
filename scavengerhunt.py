import os
import numpy as np
import cv2
import sys
import smtplib

def diffImg(t0, t1, t2):
    d1 = cv2.absdiff(t2, t1)
    d2 = cv2.absdiff(t1, t0)
    return cv2.bitwise_and(d1, d2)

def sendMessage(to, what):
    server = smtplib.SMTP("smtp.gmail.com", 587)
    server.ehlo()
    server.starttls()
    server.login("scientistscavenger2015", "StanleyScience")
    server.sendmail("ScavengerHunt", to, what)
    server.close()

MIN_MATCH_COUNT = 5
originals_path = '../../Google Drive/ScavengerHunt/Originals/'
uploads_path = '../../Google Drive/ScavengerHunt/Uploads/'
output_path = '../../Google Drive/ScavengerHunt/Processed/'

orb = cv2.ORB_create()
img1 = cv2.imread(originals_path+'Fish_noBackgroundBlack.png',0)

#img1 = cv2.imread('ozarkasmall.png',0)
#print "image one is ", type(img1), img1
h,w = img1.shape
img2 = cv2.imread(originals_path+'Armadillo_noBackgroundBlack.png',0)
#img2 = cv2.imread('backboneblacksmall.png',0)
#keypoints of image1
kp1, des1 = orb.detectAndCompute(img1,None)
bbkp, bbdes = orb.detectAndCompute(img2,None)

bf = cv2.BFMatcher()
cam = cv2.VideoCapture(0)
winName = "Object Recognition Video"
cv2.namedWindow(winName, cv2.WINDOW_AUTOSIZE)
# Read three images first:
t_minus = cv2.cvtColor(cam.read()[1], cv2.COLOR_RGB2GRAY)
t = cv2.cvtColor(cam.read()[1], cv2.COLOR_RGB2GRAY)
t_plus = cv2.cvtColor(cam.read()[1], cv2.COLOR_RGB2GRAY)
faces = []
matches = []
cr = []
i = 0
img3 = img2
found = "none"
print type(bf)
while True:

    for name in os.listdir(uploads_path):
        found = "Not Found "
        if name.endswith(".jpg"):
            print(name.split("_")[:-1][0])

            img2 = cam.read()[1]
            img2 = cv2.imread(uploads_path+name,0)
            print "Removing ", uploads_path+name
            try:
                os.remove(uploads_path+name)
            except:
                print "Error! file ", uploads_path+name
            kp2, des2 = orb.detectAndCompute(img2,None)
            matches = bf.knnMatch(des1,des2,k=2)
            matchesbb = bf.knnMatch(bbdes,des2,k=2)
            good = []
            for m,n in matches:
                if m.distance < 0.7*n.distance:
                    good.append(m)
            goodbb = []
            text = "Fish"
            for m,n in matchesbb:
                if m.distance < 0.7*n.distance:
                    goodbb.append(m)
            #print "number of good matches", len(good)
            if len(goodbb) > len(good):
                good = goodbb
                text = "Armadillo"
                print text, "length ", len(goodbb)
                kp1 = bbkp
            if len(good)>MIN_MATCH_COUNT:
                src_pts = np.float32([ kp1[m.queryIdx].pt for m in good ])
                src_pts = np.reshape(src_pts, (-1, 1, 2))
                dst_pts = np.float32([ kp2[m.trainIdx].pt for m in good ])
                dst_pts = np.reshape(dst_pts, (-1, 1, 2))
                print "got here as", text
                M, mask = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC, 10.0)
                if M is not None:
                    pts = np.float32([ [0,0],[0,h-1],[w-1,h-1],[w-1,0] ])
                    pts = np.reshape(pts, (-1,1,2))
                    dst = cv2.perspectiveTransform(pts,M)
                    found = "Success " + text + " "
                    #img2 = cv2.polylines(img2,[np.int32(dst)],True,255,3, cv2.LINE_AA)
                    cv2.putText(img2,text, (10,200), cv2.FONT_HERSHEY_SIMPLEX, 1, (0,0,255), 2)
            try:
                sendMessage(name.split("_")[:-1][0]+"@gmail.com",found)
            except:
                print "could note reach", name.split("_")[:-1][0]+"@gmail.com"
            img3 = cv2.drawKeypoints(img2,kp2[:100],img3,color=(255,0,0),flags=2)
            #img3 = cv2.drawMatches(img1,kp1,img2,kp2,matches[:10], img3, flags=2)
            #cv2.imshow( winName,  img3)
            cv2.imshow( winName,  img3)
            cv2.imwrite(output_path+found+name ,  img3)
    key = cv2.waitKey(10)
    if key == 27:
        cv2.destroyWindow(winName)
        break
print "Goodbye"
